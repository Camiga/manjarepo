// Script to print information from repo.manjaro.org

// node-fetch is used to download page HTML.
const fetch = require('node-fetch');
// htmlparser2 is used to sort through the downloaded HTML and print important parts.
const htmlparser2 = require('htmlparser2');

// :: Editable variables for storing data temporarily.
// Determine if the text is the "Generated on ..." message.
let generated = false;
// Determine if a piece of data is a table row. (<tr>)
let mirror = false;
// Determine if a piece of data is a column in the table. (<td>)
let mirrorData = false;
// Store all columns read for that row of data here.
let Collected = [];
// Store the number of fully synced branches for each mirror.
let synced = 0;
// Store each country to determine if their names need to be printed.
let lastCountry;

// Function to print all stored data nicely.
function format() {
  // :: Determine what color the output should be printed in for each mirror.
  // Default color is red, for when upToDate = 0 and no further statements are run.
  let color = '\x1b[91m';
  // If all three branches are fully synced, set the color to green.
  if (synced >= 3) color = '\x1b[92m';
  // If at least one branch is synced, set the color to yellow.
  else if (synced > 0) color = '\x1b[93m';
  // Finally, if the branches are unreachable, set the color to light gray.
  else if (synced < 0) color = '\x1b[38:5:244m';

  // Format branch status and time since last full sync.
  const branchStatus = `( ${Collected.splice(5, 8).join(' ')} last sync ${
    Collected[4]
  })`;
  // Format link by removing protocol ("*://") and replacing it with all other protocols.
  const link = `[ ${Collected[3]} ]${Collected[0].replace(
    /(.*:\/\/)/g,
    '://'
  )}`;
  // Store all data above for when printing mirror information/
  const mirrorInfo = `   ${color}⤷ ${branchStatus} ${link}\x1b[0m`;

  // If the country name has already been printed, just print the mirror name.
  if (lastCountry == Collected[2]) {
    console.log(mirrorInfo);
  }
  // If a new country hasn't been printed yet...
  else {
    // Ensure that this new country replaces the old one.
    lastCountry = Collected[2];
    // Print the country name, along with the mirror information below it.
    console.log(`${color}=> ${lastCountry}:\n${mirrorInfo}`);
  }

  // Reset the column array and color variable for the next row in the table of mirrors.
  Collected = [];
  synced = 0;
}

// Main parser begins here.
const parser = new htmlparser2.Parser({
  onopentag(name, attribs) {
    // On [<span class="navbar-text"], mark it as the date generated.
    if (name == 'span' && attribs.class == 'navbar-text') generated = true;
    // If a URL is spotted, add it to the mirror data array as it contains the mirror URL.
    if (name == 'a' && attribs.href) Collected.push(attribs.href);
    // On "<tr " check if a class exists and mark as a row of a mirror if so.
    // attribs.class ensures that we don't mark the table title as a mirror.
    if (name == 'tr' && attribs.class) mirror = true;
    // On "<td " check if the table column is part of a table row and stores mirror info.
    if (name == 'td' && mirror) {
      // Mark the text as mirror information.
      mirrorData = true;

      // <td> elements containing a class contain the status of each branch in order down.
      // If the branch is fully synced, push a tick symbol.
      if (attribs.class == 'up') {
        Collected.push('✓');
        // For "up" add to number of synced branches to color the output yellow, or green.
        ++synced;
      }
      // If the branch is not fully synced, push a cross symbol.
      if (attribs.class == 'out') Collected.push('X');
      // If the branch is unreachable, push a question mark.
      if (attribs.class == 'unknown') {
        Collected.push('?');
        // For "unknown" remove from number of synced branches to color the output grey.
        --synced;
      }
    }
  },
  ontext(text) {
    // Handle the generated date and title.
    if (generated) {
      generated = false;
      // Print program title with blue color and generated date text.
      console.log(`\n:: Manjaro Repository Status of Mirrors (${text})`);
    }
    // If a column from the mirror table is being read...
    if (mirror && mirrorData) {
      // Reset the mirror column variable.
      mirrorData = false;
      // Add any text collected to an array storing all information from that row.
      Collected.push(text);
    }
  },
  onclosetag(name) {
    // Once the row for one mirror has reached the end...
    if (name == 'tr' && mirror) {
      // Reset the '<tr ' variable.
      mirror = false;
      // Run the format function to print mirror data nicely in the terminal, and reset.
      format();
    }
  }
});

// Async is required to process main code.
async function Run() {
  // Fetch the page and save it as plaintext / HTML.
  const source = await fetch('https://repo.manjaro.org').then(res =>
    res.text()
  );
  // Write the page contents to the parser, where the printing is handled.
  await parser.write(source);
  // Close the parser once done.
  await parser.end();
}

// Ensure the code above is run.
Run();
